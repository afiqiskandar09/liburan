<?php

class Data_model extends CI_Model
{
    public function list_vacation() {
        $this->db->select("id_vacation, rute_from, rute_to, date_format(jadwal_from, '%d %M %Y %T') AS jadwal_from, date_format(jadwal_to, '%d %M %Y %T') AS jadwal_to, kendaraan, slot, slot_in");
        $this->db->from('vacation');
        $this->db->order_by('jadwal_from', 'DESC');
        $hasil = $this->db->get();
        return $hasil;
    }

    public function rute($id) {
        $this->db->select("id_vacation, rute_from, rute_to, date_format(jadwal_from, '%d %M %Y %T') AS jadwal_from, date_format(jadwal_to, '%d %M %Y %T') AS jadwal_to, kendaraan, slot, slot_in");
        $this->db->from('vacation');
        $this->db->where('id_vacation', $id);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function list_peserta_by_id_vacation($id) {
        $this->db->from('peserta');
        $this->db->where('id_vacation', $id);
        $hasil = $this->db->get();
        return $hasil;
    }
    
    public function list_peserta_by_kode($kode) {
        $this->db->from('peserta');
        $this->db->where('kode_boking', $kode);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function cek_slot($id_vacation) {
        $this->db->select('slot, slot_in');
        $this->db->from('vacation');
        $this->db->where('id_vacation', $id_vacation);
        $hasil = $this->db->get();
        return $hasil;
    }
}


?>