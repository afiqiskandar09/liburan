<?php 

class Crud_model extends CI_Model {

    // function simpan data dengan retrun id
    function save_with_id($data, $tableName) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    // function simpan data
    function save($data, $tableName) {
        $this->db->insert($tableName, $data);

        return $this->db->affected_rows();
    }

    // function update data
    function update($data, $where, $tableName) {
        $this->db->update($tableName, $data, $where);

        return $this->db->affected_rows();
    }

    function save_batch($data, $tableName) {
        $this->db->insert_batch($tableName, $data);
        return $this->db->affected_rows();
    }

    function delete($where, $tableName) {
        $this->db->delete($tableName, $where);
        return $this->db->affected_rows();
    }

}