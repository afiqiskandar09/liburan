<?php

class Dashboard_model extends CI_Model
{
    public function cek_nik($nik) {
        $this->db->from('peserta');
        $this->db->like('nik', $nik);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function get_by_kdboking($kdboking) {
        $this->db->from('peserta');
        $this->db->where('kode_boking', $kdboking);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function peserta_fil($x) {
        $this->db->from('peserta');
        if ($x == '0') {
            $this->db->where('status', '0');
        } elseif ($x == '1') {
            $this->db->where('status', '1');
        } elseif ($x == '2') {
            $this->db->where('status', '2');
        }
        $hasil = $this->db->get();
        return $hasil;
    }
}


?>