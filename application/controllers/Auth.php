<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct() {
		parent::__construct();

		// Load model
		$this->load->model('Auth_model', 'auth_m');
	}
	
	// ==================================================================================================================
	// --- View
	// ==================================================================================================================

    public function login() {
		// Check if you have login
		if ($this->session->userdata('user_id') != '') {
			redirect(base_url() . 'dashboard');
		}
		$data['title'] = 'Login';
		$this->load->view("login", $data);
	}

	// ==================================================================================================================
	// --- Proses Data
	// ==================================================================================================================
	
	public function login_validation() {
		$username	= $this->input->post('username');
		$username_h	= hash('sha512', $username);
		$password 	= $this->input->post('password');

		$row = $this->auth_m->check_users($username_h);

		if ($row->num_rows() > 0) {
			$fetch = $row->row_array();

			$password_h	= hash('sha512', $password);
			$hash     	= hash('sha512', $password_h . $fetch['uniq_code']);
			if ($hash == $fetch['password']) {
				// Set session to array
				$session_data = array(
					'user_id' 		=> $fetch['id'],
					'user_fullname' => $fetch['fullname']
				);

				$this->session->set_userdata($session_data);

				$response = [
					'msg'   => 'Berhasil login',
					'url'   => base_url().'dashboard'
				];
			} else {
				$response = [
					'msg'   => 'Gagal login'
				];
			}
		} else {
			$response = [
				'msg'   => 'Tidak terdaftar'
			];
		}

		header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
	} 

	public function logout() {
		$this->session->unset_userdata('user_id');
		session_destroy();
		redirect(base_url() . 'auth/login');
	}
}


?>