<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends MY_Controller
{
    public function __construct() {
		parent::__construct();

		// Load model
		$this->load->model('Data_model', 'data_m');
		$this->load->model('Crud_model', 'crud');
	}
	
	// ==================================================================================================================
	// --- View
	// ==================================================================================================================
    
    public function index() {
		if ($this->session->userdata('user_id') == '') {
            redirect(base_url() . 'dashboard');
		}
        $data['title'] = 'Data';
        $data['add_css'] = [
            "assets/vendors/custom/datatables/datatables.bundle.css"
        ];
        $data['add_js'] = [
            "assets/vendors/custom/datatables/datatables.bundle.js",
            "assets/action_js/master_data.js"
        ];
		$this->render_page('master_data', $data);
    }

    public function list_peserta($id_vacation) {
        if ($this->session->userdata('user_id') == '') {
            redirect(base_url() . 'dashboard');
        }
        $data['idVacation'] = $id_vacation;
        $data['getDataVacation'] = $this->data_m->rute($id_vacation)->row_array();
        $data['title'] = 'Data';
        $data['add_css'] = [
            "assets/vendors/custom/datatables/datatables.bundle.css"
        ];
        $data['add_js'] = [
            "assets/vendors/custom/datatables/datatables.bundle.js",
            "assets/action_js/list_peserta.js"
        ];
		$this->render_page('list_peserta', $data);
    }
    
    // ==================================================================================================================
    // --- View
    // ==================================================================================================================

    public function getDataVacation() {
        $data = $this->data_m->list_vacation()->result_array();

        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function prosesData() {
        $id_vacation    = $this->input->post('id_vacation');
        $rute_from      = $this->input->post('rute_from');
        $rute_to        = $this->input->post('rute_to');
        $jadwal_from    = $this->input->post('jadwal_from');
        $jadwal_to      = $this->input->post('jadwal_to');
        $kendaraan      = $this->input->post('kendaraan');
        $slot           = $this->input->post('slot');
        
        if ($id_vacation != "") { //update
            // $data = 
        } else { //insert
            $data = [
                'rute_from'     => $rute_from,
                'rute_to'       => $rute_to,
                'jadwal_from'   => $jadwal_from,
                'jadwal_to'     => $jadwal_to,
                'kendaraan'     => $kendaraan,
                'slot'          => $slot,
                'slot_in'       => $slot
            ];

            $this->crud->save($data, 'vacation');

            $response = [
                'status'    => 200,
                'code'      => '1',
                'msg'       => 'berhasil'
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function hapusData($id) {
        $where = ['id_vacation' => $id];

        if ($this->crud->delete($where, 'vacation') > 0) {
            $this->crud->delete($where, 'peserta');
            $response = [
                'status'    => 200,
                'msg'       => 'berhasil'
            ];
        } else {
            $response = [
                'status'    => 500,
                'msg'       => 'gagal'
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function getDataPesertaVacation($id) {
        $data = $this->data_m->list_peserta_by_id_vacation($id)->result_array();

        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function getDataPesertaVacationByKode($kode) {
        $data = $this->data_m->list_peserta_by_kode($kode)->result_array();

        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function verifikasiPeserta() {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $id_vacation = $this->input->post('id_vacation');

        $cek = $this->data_m->cek_slot($id_vacation)->row_array();
        
        if ($cek['slot_in'] == '0') {
            $response = [
                'status'    => 200,
                'msg'       => 'gagal'
            ];
        } else {
            if ($status == '1') {
                $jml = $cek['slot_in'] - 1;
                $whereV = ['id_vacation' => $id_vacation];
                $dataV  = ['slot_in' => $jml];
                $this->crud->update($dataV, $whereV, 'vacation');
            } elseif ($status == '2') {
                if ($cek['slot_in'] < $cek['slot']) {
                    $jml = $cek['slot_in'] + 1;
                    $whereV = ['id_vacation' => $id_vacation];
                    $dataV  = ['slot_in' => $jml];
                    $this->crud->update($dataV, $whereV, 'vacation');
                } else {
                    $jml = $cek['slot_in'];
                }
            }
            
            $whereP = ['id' => $id];
            $dataP  = ['status'    => $status];

            $this->crud->update($dataP, $whereP, 'peserta');

            $response = [
                'status'    => 200,
                'msg'       => 'berhasil',
                'jml'       => $jml
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function vacationByKode($kode) {
        $data = $this->data_m->list_peserta_by_kode($kode)->row_array();
        $vac  = $this->data_m->rute($data['id_vacation'])->row_array();

        header('Content-Type: application/json');
        echo json_encode($vac, JSON_PRETTY_PRINT);
    }
}


?>