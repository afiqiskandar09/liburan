<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct() {
		parent::__construct();

		// Load model
		$this->load->model('Dashboard_model', 'dashboard');
		$this->load->model('crud_model', 'crud');
    }

    public function index() {
        if ($this->session->userdata('user_id') != '') {
            $data['psr'] = $this->dashboard->peserta_fil('')->num_rows();
            $data['blm'] = $this->dashboard->peserta_fil('0')->num_rows();
            $data['stj'] = $this->dashboard->peserta_fil('1')->num_rows();
            $data['tlk'] = $this->dashboard->peserta_fil('2')->num_rows();
            $data['title'] = "Dashboard";
            $data['add_css'] = [
                "assets/vendors/custom/fullcalendar/fullcalendar.bundle.css"
            ];
            $data['add_js'] = [
                "assets/vendors/custom/fullcalendar/fullcalendar.bundle.js",
                "assets/action_js/dashboard.js"
            ];
            $this->render_page('dashboard', $data);
        } else {
            // redirect('login');
            $data['title'] = "Dashboard";
            $data['add_css'] = [
                "assets/vendors/custom/datatables/datatables.bundle.css"
            ];
            $data['add_js'] = [
                "assets/vendors/custom/datatables/datatables.bundle.js",
                "assets/action_js/dashboard_user.js"
            ];
            $this->render_page('daftar_liburan', $data);
        }
    }

    public function daftar_vacation($id) {
        if ($this->session->userdata('user_id') != '') {
            redirect(base_url() . 'dashboard');
        } else {
            $data['title']          = "Pendaftaran";
            $data['id_vacation']    = $id;
            $data['add_js'] = [
                // "assets/vendors/custom/datatables/datatables.bundle.js",
                "assets/action_js/pendaftaran.js"
            ];
            $this->render_page('pendaftaran', $data);
        }
    }

    public function prosesDataPendaftaran() {
        $fullname = $this->input->post('fullname');
        $nik = $this->input->post('nik');
        $id_vacation = $this->input->post('id_vacation');
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, 8) as $k) $rand .= $seed[$k];
        $kodeBoking = 'BOOK-'.$rand;

        foreach ($fullname as $key => $val) {
            $cekNik = $this->dashboard->cek_nik($val)->num_rows();

            if ($cekNik < 1) {
                $data = [
                    'nik'           => $nik[$key],
                    'fullname'      => $val,
                    'id_vacation'   => $id_vacation,
                    'kode_boking'   => $kodeBoking
                ];

                $save[] = $data;
            } else {
                continue;
            }
        }

        if ($this->crud->save_batch($save, 'peserta')) {
            $response = [
                'status'    => 200,
                'msg'       => 'berhasil',
                'url'       => base_url() . 'done/berhasil/'.$kodeBoking
            ];
        } else {
            $response = [
                'status'    => 200,
                'msg'       => 'gagal'
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);

    }
}


?>