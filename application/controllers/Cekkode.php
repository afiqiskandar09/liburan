<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cekkode extends MY_Controller
{
    public function __construct() {
		parent::__construct();

		// Load model
		$this->load->model('Data_model', 'data_m');
    }
    
    public function index() {
        if ($this->session->userdata('user_id') != '') {
            redirect(base_url() . 'dashboard');
		}
        $data['title'] = 'Cek Kode Boking';
        $data['add_css'] = [
            "assets/vendors/custom/datatables/datatables.bundle.css"
        ];
        $data['add_js'] = [
            "assets/vendors/custom/datatables/datatables.bundle.js",
            "assets/action_js/cek.js"
        ];
		$this->render_page('cek', $data);
    }
}


?>