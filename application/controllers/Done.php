<?php

class Done extends CI_Controller
{
    public function __construct() {
		parent::__construct();

		// Load model
		$this->load->model('Dashboard_model', 'dashboard');
    }

    public function berhasil($kdBoking) {
        $data['kdBoking']   = $kdBoking;
        $data['listData']   = $this->dashboard->get_by_kdboking($kdBoking)->row_array();
        $this->load->view('done', $data);
    }
}


?>