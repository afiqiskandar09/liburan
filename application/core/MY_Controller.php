<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    function render_page($content, $data = NULL) {
        $user_id    = $this->session->userdata('user_id');

        $data['assets_css'] = $this->load->view('template/assets_css', $data, TRUE);
        $data['assets_js'] = $this->load->view('template/assets_js', $data, TRUE);
        if ($user_id != '') {
            $data['navbar'] = $this->load->view('template/navbar_admin', $data, TRUE);
            $data['sidebar'] = $this->load->view('template/sidebar_admin', $data, TRUE);
        } else {
            $data['navbar'] = $this->load->view('template/navbar_user', $data, TRUE);
            $data['sidebar'] = $this->load->view('template/sidebar_user', $data, TRUE);
        }
        $data['content'] = $this->load->view($content, $data, TRUE);
        $data['footer'] = $this->load->view('template/footer', $data, TRUE);

        $this->load->view('template_base', $data);
    }
}


?>