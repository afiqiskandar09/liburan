<div class="m-portlet m-portlet--mobile" id="group-table-absensi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					List Vacation
				</h3>
			</div>
		</div>
	</div>
    <form class="m-form m-form--fit m-form--label-align-right" id="formSave">
        <div class="m-portlet__body">
            <input type="hidden" name="id_vacation" value="<?=$id_vacation?>">
            <div id="post">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6 group-invalid">
                        <label>Full Name:</label>
                        <input type="text" class="form-control m-input" id="fullname_1" name="fullname[1]" placeholder="Masukan nama peserta sesuai KTP" autocomplete="off">
                    </div>
                    <div class="col-lg-6 group-invalid">
                        <label class="">NIK:</label>
                        <input type="text" class="form-control m-input" id="nik_1" name="nik[1]" placeholder="Masukan NIK peserta" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 mb-4">
            <button type="button" id="tambah_field" class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-container="body" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Tambah Pertanyaan">
                <i class="la la-plus-circle"></i>
            </button>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <button type="submit" class="btn btn-success pull-right" id="save"> <i class="la la-save mr-2"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>