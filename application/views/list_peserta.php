<div class="m-portlet m-portlet--mobile" id="group-table-absensi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					List Peserta
				</h3>
			</div>
		</div>
	</div>
    <!--begin::Form-->
    <!-- <button type="button" class="btn btn-outline-warning m-btn m-btn--icon mt-3 ml-4" onclick="kembaliAbsensi()">
        <span>
            <i class="la la-arrow-circle-o-left"></i>
            <span>Kembali</span>
        </span>
    </button> -->
    <div style="margin-top: 20px; margin-left: 30px;">
        <table>
            <tr>
                <td><?=$getDataVacation['rute_from']?><br><?=$getDataVacation['jadwal_from']?></td>
                <td><i class="la la-long-arrow-right"></i></td>
                <td><?=$getDataVacation['rute_to']?><br><?=$getDataVacation['jadwal_to']?></td>
            </tr>
            <h5 id="slot">Slot : <?=$getDataVacation['slot_in']?></h5>
        </table>
    </div>
	<div class="m-portlet__body">
        
        <table class="table table-striped- table-bordered table-hover table-checkable" id="peserta">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Kode Boking</th>
                    <th>Status</th>
                    <th>#</th>
                </tr>
            </thead>


        </table>
		<!--end: Datatable -->
	</div>
</div>

<script type="text/javascript">
    var idVacation = '<?=$idVacation?>';
</script>