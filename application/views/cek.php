<div class="m-portlet m-portlet--mobile" id="group-table-absensi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Cek Kode Boking
				</h3>
			</div>
		</div>
	</div>
    <form class="m-form m-form--fit m-form--label-align-right" id="formSave">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-12 group-invalid">
                    <label>Kode Boking:</label>
                    <input type="text" class="form-control m-input" id="kode_boking" name="kode_boking" placeholder="Masukan Kode Boking Anda" autocomplete="off">
                </div>
            </div>

            <div id="append" style="marigin-top: 20px; margin-left: 30px; margin-bottom: 40px;"></div>

            <table class="table table-striped- table-bordered table-hover table-checkable" id="peserta">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Status</th>
                    </tr>
                </thead>


            </table>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <button type="button" class="btn btn-success pull-right" id="save" onclick="filter()"> <i class="la la-save mr-2"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>