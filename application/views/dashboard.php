<div class="row">
    <!-- Jummlah Status Pending -->
    <div class="col-md-3">
        <div class="card bg-warning-400" id="div-jumlah-status-pending">
            <div class="card-body">
                <div class="d-flex">
                    <h3 class="font-weight-semibold mb-0" id="jumlah-status-pending"><?=$psr?></h3>
                </div>
                
                <div>
                    Total Peserta
                </div>
            </div>
        </div>
    </div>
    <!-- /jumlah status pending -->
    
    <!-- Jummlah Status Proses -->
    <div class="col-md-3">
        <div class="card bg-info-400" id="div-jumlah-status-proses">
            <div class="card-body">
                <div class="d-flex">
                    <h3 class="font-weight-semibold mb-0" id="jumlah-status-proses"><?=$blm?></h3>
                </div>
                
                <div>
                    Total Belum Diresponse
                </div>
            </div>
        </div>
    </div>
    <!-- /jumlah status proses -->

    <!-- Jummlah Status Kirim -->
    <div class="col-md-3">
        <div class="card bg-teal-400" id="div-jumlah-status-kirim">
            <div class="card-body">
                <div class="d-flex">
                    <h3 class="font-weight-semibold mb-0" id="jumlah-status-kirim"><?=$stj?></h3>
                </div>
                
                <div>
                    Total Disetujui
                </div>
            </div>
        </div>
    </div>
    <!-- /jumlah status kirim -->

    <!-- Jummlah Status Selesai -->
    <div class="col-md-3">
        <div class="card bg-success-400" id="div-jumlah-status-selesai">
            <div class="card-body">
                <div class="d-flex">
                    <h3 class="font-weight-semibold mb-0" id="jumlah-status-selesai"><?=$tlk?></h3>
                </div>
                
                <div>
                    Total Ditolak
                </div>
            </div>
        </div>
    </div>
    <!-- /jumlah status selesai -->
</div>