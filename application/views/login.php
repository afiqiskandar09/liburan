<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
	<meta charset="utf-8" />
	<title>Liburan | Login Page</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

	<!--begin::Web font -->
	<script src="<?=base_url()?>ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Web font -->

	<!--begin::Global Theme Styles -->
	<link href="<?=base_url()?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles -->

	<link rel="shortcut icon" href="<?=base_url()?>assets/demo/default/media/img/logo/favicon.ico" />
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
			<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
				<div class="m-stack m-stack--hor m-stack--desktop">
					<div class="m-stack__item m-stack__item--fluid">
						<div class="m-login__wrapper">
							<div class="m-login__logo">
								<a href="#">
									<img src="<?=base_url()?>assets/app/media/img/logos/logo-2.png">
								</a>
							</div>
							<div class="m-login__signin">
								<div class="m-login__head">
									<h3 class="m-login__title">Sign In To Admin</h3>
								</div>
								<form class="m-login__form m-form" action="#">
									<div class="form-group m-form__group">
										<input class="form-control m-input" type="text" placeholder="Username" name="username" autocomplete="off">
									</div>
									<div class="form-group m-form__group">
										<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
									</div>
									<div class="m-login__form-action">
										<button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Sign In</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center"
				style="background-image: url(<?=base_url()?>assets/app/media/img/bg/bg-4.jpg)">
				<div class="m-grid__item">
					<h3 class="m-login__welcome">Join Our Community</h3>
					<p class="m-login__msg">
						Lorem ipsum dolor sit amet, coectetuer adipiscing<br>elit sed diam nonummy et nibh euismod
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	<!--begin::Global Theme Bundle -->
	<script src="<?=base_url()?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Global Theme Bundle -->

	<script type="text/javascript">
        var site_url = '<?php echo site_url(); ?>';
    </script>

	<!--begin::Page Scripts -->
	<script src="<?=base_url()?>assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>
	<!--end::Page Scripts -->
</body>
<!-- end::Body -->
</html>