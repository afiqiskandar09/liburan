<div class="m-portlet m-portlet--mobile" id="group-table-absensi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					List Vacation
				</h3>
			</div>
		</div>
	</div>
    <!--begin::Form-->
    <!-- <button type="button" class="btn btn-outline-warning m-btn m-btn--icon mt-3 ml-4" onclick="kembaliAbsensi()">
        <span>
            <i class="la la-arrow-circle-o-left"></i>
            <span>Kembali</span>
        </span>
    </button> -->
    <div class="col-xl-12 order-1 order-xl-2 mt-4 m--align-right">
        <button type="button" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--square m-btn--pill" onclick="tambah()">
            <span>
                <i class="la flaticon-plus"></i>
                <span>
                    Tambah Data
                </span>
            </span>
        </button>
        <div class="m-separator m-separator--dashed d-xl-none"></div>
    </div>
	<div class="m-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="vacation">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Rute</th>
                    <th>Transportasi</th>
                    <th>Slot</th>
                    <th>#</th>
                </tr>
            </thead>


        </table>
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="modalCard" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="reset" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" id="formSave">
                <input type="hidden" name="id_vacation">
                <div class="modal-body">
                    <div class="form-group m-form__group row m--margin-top-20 group-invalid">
                        <label class="col-form-label col-lg-3 col-sm-12">Rute Keberangkatan</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="rute_from" id="rute_from" data-z-index="1100" placeholder="Tuliskan rute mulai keberngkatan" autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--margin-top-20 group-invalid">
                        <label class="col-form-label col-lg-3 col-sm-12">Rute Tujuan</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="rute_to" id="rute_to" data-z-index="1100" placeholder="Tuliskan rute tujuan" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--margin-top-20 group-invalid">
                        <label class="col-form-label col-lg-3 col-sm-12">Jadwal Keberangkatan</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="jadwal_from" id="jadwal_from" readonly data-z-index="1100" placeholder="Pilih jadwal keberangkatan" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--margin-top-20 group-invalid">
                        <label class="col-form-label col-lg-3 col-sm-12">Jadwal Sampai</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="jadwal_to" id="jadwal_to" readonly data-z-index="1100" placeholder="Pilih jadwal sampai tujuan" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--margin-top-20 group-invalid">
                        <label class="col-form-label col-lg-3 col-sm-12">Kendaraan</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select name="kendaraan" id="kendaraan" class="form-control" data-z-index="1100">
                                <option value="">-- PILIH --</option>
                                <option value="Bus">Bus</option>
                                <option value="Kapal">Kapal</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--margin-top-20">
                        <label class="col-form-label col-lg-3 col-sm-12">Slot Penumpang</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="slot" id="slot" data-z-index="1100" placeholder="Masukkan jumlah maksimal penumpang" autocomplete="off"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary m-btn" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brand m-btn" id="saveButton"><span><i class="la la-save"></i> <span>Save</span></span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->