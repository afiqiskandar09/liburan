<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
	<meta charset="utf-8" />

	<title>Liburan | Selamat!</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

	<!--begin::Web font -->
	<script src="<?=base_url();?>ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Web font -->
    
    <!--begin::Global Theme Styles -->
    <link href="<?=base_url()?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->
</head>
<!-- end::Head -->


<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <div class="m-portlet m-portlet--mobile" id="group-table-absensi">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <a href="<?=base_url().'dashboard'?>" class="btn btn-outline-warning m-btn m-btn--icon mt-3 ml-4">
            <span>
                <i class="la la-arrow-circle-o-left"></i>
                <span>Kembali</span>
            </span>
        </a>
        <div class="m-portlet__body">
            <center>
                <h1 class="text-success">Berhasil!</h1>
                <p class="text-primary">Simpan kode boking anda : <strong><?=$kdBoking?> </strong></p>
                <p class="text-primary">Harap cek verifikasi pada menu verifikasi. Harap cek selalu 1 minggu sebelum keberangkatan. Dan harap membawa KTP untuk keperluan verifikasi kembali</p>
            </center>
        </div>
    </div>
	
	<!--begin::Global Theme Bundle -->
    <script src="<?=base_url()?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
</body>
<!-- end::Body -->

<!-- Mirrored from keenthemes.com/metronic/preview/?page=index&demo=default by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Sep 2018 05:14:37 GMT -->

</html>