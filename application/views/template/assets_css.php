<!--begin::Global Theme Styles -->
<link href="<?=base_url()?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Page Vendors Styles -->
<?php
if (isset($add_css)) {
    foreach ($add_css as $key) {
        echo "\n";
        echo '<link href="'.base_url($key).'" rel="stylesheet" type="text/css">';
    }
}
?>
<!--end::Page Vendors Styles -->