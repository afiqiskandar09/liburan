<footer class="m-grid__item m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
            </div>
            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
				Created by <a href="#" class="m-link">Afiq Iskandar</a>
            </div>
        </div>
    </div>
</footer>