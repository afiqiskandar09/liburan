<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
	<meta charset="utf-8" />

	<title>Liburan | <?=$title?></title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

	<!--begin::Web font -->
	<script src="<?=base_url();?>ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Web font -->
    
    <?php echo $assets_css; ?>
</head>
<!-- end::Head -->


<!-- begin::Body -->

<body
	class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- BEGIN: Header -->
        <?php echo $navbar; ?>
		<!-- END: Header -->
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
			<!-- BEGIN: Left Aside -->
			<?php echo $sidebar; ?>
			<!-- END: Left Aside -->
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<!-- <div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title ">Dashboard</h3>
						</div>
					</div>
				</div> -->
				<!-- END: Subheader -->
				<div class="m-content">
                    <?php echo $content; ?>
				</div>
			</div>
		</div>
		<!-- end:: Body -->

		<!-- begin::Footer -->
		<?= $footer; ?>
		<!-- end::Footer -->


	</div>
	<!-- end:: Page -->

	
	<!-- begin::Scroll Top -->
	<div id="m_scroll_top" class="m-scroll-top">
		<i class="la la-arrow-up"></i>
	</div>
	<!-- end::Scroll Top -->
	
	<?= $assets_js; ?>
</body>
<!-- end::Body -->

<!-- Mirrored from keenthemes.com/metronic/preview/?page=index&demo=default by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Sep 2018 05:14:37 GMT -->

</html>