<div class="m-portlet m-portlet--mobile" id="group-table-absensi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					List Vacation
				</h3>
			</div>
		</div>
	</div>
    <!--begin::Form-->
    <!-- <button type="button" class="btn btn-outline-warning m-btn m-btn--icon mt-3 ml-4" onclick="kembaliAbsensi()">
        <span>
            <i class="la la-arrow-circle-o-left"></i>
            <span>Kembali</span>
        </span>
    </button> -->
	<div class="m-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="vacation">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Rute</th>
                    <th>Transportasi</th>
                    <th>#</th>
                </tr>
            </thead>
        </table>
		<!--end: Datatable -->
	</div>
</div>