function loadTable() {
    $('#vacation').DataTable({
        destroy: true,
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
        ajax: {
            url: site_url + 'data/getDataVacation',
            dataSrc: ""
        },
        columns: [
            {data: null, render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            {data: null, render: function(data, type, row) {
                output = '<table>\
                    <tr>\
                        <td>'+data.rute_from+'<br>'+data.jadwal_from+'</td>\
                        <td><i class="la la-long-arrow-right"></i></td>\
                        <td>'+data.rute_to+'<br>'+data.jadwal_to+'</td>\
                    </tr>\
                </table>';
                return output;
            }},
            {data: "kendaraan"},
            {
                data: null,
                targets: -1,
                orderable: !1,
                render: function(row, a, i) {
                    var base = '<a href="'+site_url+'dashboard/daftar_vacation/'+row.id_vacation+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Daftar">\
                        <i class="la la-file-text-o"></i>\
                    </a>';
                return base;
            }}
        ]
    });
}

jQuery(document).ready(function () {
    loadTable();
});