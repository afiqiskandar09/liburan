$(function(){
    $("#jadwal_from").datetimepicker({
        todayHighlight: !0,
        autoclose: !0,
        format: "yyyy-mm-dd hh:ii"
    });

    $("#jadwal_to").datetimepicker({
        todayHighlight: !0,
        autoclose: !0,
        format: "yyyy-mm-dd hh:ii"
    });

    $("#formSave").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            error.addClass('form-control-feedback text-danger');
            element.closest('.group-invalid').append(error);
        },
        rules: {
            rute_from: {
                required: !0
            },
            rute_to: {
                required: !0
            },
            jadwal_from: {
                required: !0
            },
            jadwal_to: {
                required: !0
            },
            kendaraan: {
                required: !0
            },
            slot: {
                required: !0
            },
        },
        submitHandler: function (e) {
            var formData = new FormData($('#formSave')[0]);

            $.ajax({
                type: 'POST',
                url: site_url + 'data/prosesData',
                data : formData,
                contentType : false,
                processData : false,
                cache: false,
                dataType : "JSON",
                success: function(data) {
                    if (data.msg == 'berhasil') {
                        $('#modalCard').modal('hide');
                        loadTable();
                        toastr.success("Berhasil menambah vacation");         
                    }
                }
            });
        }
    });
})

function loadTable() {
    $('#vacation').DataTable({
        destroy: true,
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
        ajax: {
            url: site_url + 'data/getDataVacation',
            dataSrc: ""
        },
        columns: [
            {data: null, render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            {data: null, render: function(data, type, row) {
                output = '<table>\
                    <tr>\
                        <td>'+data.rute_from+'<br>'+data.jadwal_from+'</td>\
                        <td><i class="la la-long-arrow-right"></i></td>\
                        <td>'+data.rute_to+'<br>'+data.jadwal_to+'</td>\
                    </tr>\
                </table>';
                return output;
            }},
            {data: "kendaraan"},
            {data: "slot_in"},
            {
                data: null,
                targets: -1,
                orderable: !1,
                render: function(row, a, i) {
                    var base = '<span style="overflow: visible; position: relative; width: 110px;">\
                    <div class="dropdown">\
                        <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\
                            <i class="la la-ellipsis-h"></i>\
                        </a>\
                        <div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-187px, -59px, 0px);">\
                            <a class="dropdown-item" href="'+site_url+'data/list_peserta/'+row.id_vacation+'"><i class="la la-edit"></i> List Peserta</a>\
                            <a class="dropdown-item" href="#" onclick="hapus(event,`'+row.id_vacation+'`)"><i class="la la-trash-o"></i> Hapus</a>\
                        </div>\
                    </div>\
                </span>';
                return base;
            }}
        ]
    });
}

function tambah() {
    $('#formSave')[0].reset();
    $("#saveButton").attr("disabled", false);
    $('.modal-title').text('Tambah Data');
    $('#modalCard').modal('show');
}

function hapus(e,id){
    swal({
        title: 'Anda yakin?',
        text: "Anda tidak akan bisa mengembalikan ini!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
          $.ajax({
            url: site_url + "data/hapusData/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                if (data.msg == 'berhasil') {
                    loadTable();
                    swal(
                        'Terhapus!',
                        'Vacation berhasil telah terhapus.',
                        'success'
                    );
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
          });
        }
    });
    e.preventDefault();
    // console.log(id);
}

jQuery(document).ready(function () {
    loadTable();
});