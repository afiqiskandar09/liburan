$(document).ready(function(){
    var wrapper     = $("#post");
    var add_button  = $("#tambah_field");
    var x = 2;

    $(add_button).click(function(e){
        // console.log('halloo');
        e.preventDefault();
        $(wrapper).append(
            '\
            <div>\
                <hr>\
                <button type="button" id="hapus_field" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air ml-2 mb-2" data-container="body" data-toggle="m-tooltip" data-placement="right" title="Hapus" data-original-title="Hapus">\
                    <i class="la la-trash-o"></i>\
                </button>\
                <div class="form-group m-form__group row">\
                    <div class="col-lg-6 group-invalid">\
                        <label>Full Name:</label>\
                        <input type="text" class="form-control m-input" id="fullname_'+x+'" name="fullname['+x+']" placeholder="Masukan nama peserta" autocomplete="off">\
                    </div>\
                    <div class="col-lg-6 group-invalid">\
                        <label class="">NIK:</label>\
                        <input type="text" class="form-control m-input" id="nik_'+x+'" name="nik['+x+']" placeholder="Masukan NIK peserta" autocomplete="off">\
                    </div>\
                </div>\
            </div>\
            '
        );
        x++;
    });

    $(wrapper).on('click', '#hapus_field', function(e){
        e.preventDefault();
        $(this).parent('div').remove();
    });
});

$(function(){
    $("#formSave").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            error.addClass('form-control-feedback text-danger');
            element.closest('.group-invalid').append(error);
        },
        rules: {
            fullname: {
                required: !0
            },
            nik: {
                required: !0
            },
        },
        submitHandler: function (e) {
            var formData = new FormData($('#formSave')[0]);

            $.ajax({
                type: 'POST',
                url: site_url + 'dashboard/prosesDataPendaftaran',
                data : formData,
                contentType : false,
                processData : false,
                cache: false,
                dataType : "JSON",
                success: function(data) {
                    if (data.msg == 'berhasil') {
                        window.location.href = data.url;       
                    }
                }
            });
        }
    });
})