// $(function(){
//     $("#formSave").validate({
//         ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
//         highlight: function(element, errorClass) {
//             $(element).removeClass(errorClass);
//         },
//         unhighlight: function(element, errorClass) {
//             $(element).removeClass(errorClass);
//         },
//         errorPlacement: function(error, element) {
//             error.addClass('form-control-feedback text-danger');
//             element.closest('.group-invalid').append(error);
//         },
//         rules: {
//             fullname: {
//                 required: !0
//             },
//             nik: {
//                 required: !0
//             },
//         },
//         submitHandler: function (e) {
//             var formData = new FormData($('#formSave')[0]);

//             $.ajax({
//                 type: 'POST',
//                 url: site_url + 'dashboard/prosesDataPendaftaran',
//                 data : formData,
//                 contentType : false,
//                 processData : false,
//                 cache: false,
//                 dataType : "JSON",
//                 success: function(data) {
//                     if (data.msg == 'berhasil') {
//                         window.location.href = data.url;       
//                     }
//                 }
//             });
//         }
//     });
// })

$(document).ready(function(){
    $('#peserta').hide();
})

function filter() {
    var x = $('#kode_boking').val();
    if (x == '') {
        toastr.error("Masukan kode boking anda!");
    } else {
        // loadTable(x);
        $('#append').html('');
        $.ajax({
            url: site_url + "data/vacationByKode/"+x,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#append').html('<table>\
                    <tr>\
                        <td>'+data.rute_from+'<br>'+data.jadwal_from+'</td>\
                        <td><i class="la la-long-arrow-right"></i></td>\
                        <td>'+data.rute_to+'<br>'+data.jadwal_to+'</td>\
                    </tr>\
                </table>');
                $('#peserta').show();
                loadTable(x);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
          });
    }
}

function loadTable(x) {
    $('#peserta').DataTable({
        destroy: true,
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
        ajax: {
            url: site_url + 'data/getDataPesertaVacationByKode/'+x,
            dataSrc: ""
        },
        columns: [
            {data: null, render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            {data: "nik"},
            {data: "fullname"},
            {data: null, render: function(data, type, row) {
                if (data.status == '0') {
                    output = '<span class="m-badge m-badge--warning m-badge--wide">Belum Verifikasi</span>';
                } else if(data.status == '1') {
                    output = '<span class="m-badge m-badge--success m-badge--wide">Setuju</span>';
                } else {
                    output = '<span class="m-badge m-badge--danger m-badge--wide">Tolak</span>';
                }

                return output;
            }},
        ]
    });
}