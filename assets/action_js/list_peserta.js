function loadTable() {
    $('#peserta').DataTable({
        destroy: true,
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
        ajax: {
            url: site_url + 'data/getDataPesertaVacation/'+idVacation,
            dataSrc: ""
        },
        columns: [
            {data: null, render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            {data: "nik"},
            {data: "fullname"},
            {data: "kode_boking"},
            {data: null, render: function(data, type, row) {
                if (data.status == '0') {
                    output = '<span class="m-badge m-badge--warning m-badge--wide">Belum Verifikasi</span>';
                } else if(data.status == '1') {
                    output = '<span class="m-badge m-badge--success m-badge--wide">Setuju</span>';
                } else {
                    output = '<span class="m-badge m-badge--danger m-badge--wide">Tolak</span>';
                }

                return output;
            }},
            {
                data: null,
                targets: -1,
                orderable: !1,
                render: function(row, a, i) {
                    if (row.status == '0') {
                        base = '\
                        <button type="button" onclick="pushVerifikasi(event,`'+row.id+'`, `1`)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Setuju">\
                            <i class="la la-check-circle-o"></i>\
                        </button>\
                        <button type="button" onclick="pushVerifikasi(event,`'+row.id+'`, `2`)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Tolak">\
                            <i class="la la-times-circle-o"></i>\
                        </button>\
                        ';
                    } else {
                        base = '\
                        <span class="dropdown">\
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-right">\
                                <a class="dropdown-item" href="#" onclick="pushVerifikasi(event,`'+row.id+'`, `1`)"><i class="la la-check-circle-o"></i> Setuju</a>\
                                <a class="dropdown-item" href="#" onclick="pushVerifikasi(event,`'+row.id+'`, `2`)"><i class="la la-times-circle-o"></i> Tolak</a>\
                            </div>\
                        </span>\
                        ';
                    }

                return base;
            }}
        ]
    });
}

function pushVerifikasi(e, id, status) {
    e.preventDefault();
    if (status == '1') {
        var as = 'Setuju';
    } else if(status == '2') {
        var as = 'Tolak';
    }
    swal({
        title: 'Anda yakin?',
        text: "Status akan diubah menjadi '"+as+"'",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Submit',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            $('#slot').html('');
            $.ajax({
                url: site_url + 'data/verifikasiPeserta',
                type: "POST",
                data: {'id' : id, 'status' : status, 'id_vacation':idVacation},
                dataType: 'json',
                success: function(data) {
                    if (data.msg == 'berhasil') {
                        loadTable();
                        toastr.success("Berhasil. . .");
                        $('#slot').html('Slot : '+data.jml);
                    }
                }
            });
        }
    });
}

jQuery(document).ready(function () {
    loadTable();
});