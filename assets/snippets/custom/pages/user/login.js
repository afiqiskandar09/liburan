var SnippetLogin = function () {
	var e = $("#m_login"),
		i = function (e, i, a) {
			var l = $('<div class="m-alert m-alert--outline alert alert-' + i + ' alert-dismissible" role="alert">\t\t\t<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\t\t\t<span></span>\t\t</div>');
			e.find(".alert").remove(), l.prependTo(e), mUtil.animateClass(l[0], "fadeIn animated"), l.find("span").html(a)
		},
		a = function () {
			e.removeClass("m-login--forget-password"), e.removeClass("m-login--signup"), e.addClass("m-login--signin"), mUtil.animateClass(e.find(".m-login__signin")[0], "flipInX animated")
		},
		l = function () {
			$("#m_login_forget_password").click(function (i) {
				i.preventDefault(), e.removeClass("m-login--signin"), e.removeClass("m-login--signup"), e.addClass("m-login--forget-password"), mUtil.animateClass(e.find(".m-login__forget-password")[0], "flipInX animated")
			}), $("#m_login_forget_password_cancel").click(function (e) {
				e.preventDefault(), a()
			}), $("#m_login_signup").click(function (i) {
				i.preventDefault(), e.removeClass("m-login--forget-password"), e.removeClass("m-login--signin"), e.addClass("m-login--signup"), mUtil.animateClass(e.find(".m-login__signup")[0], "flipInX animated")
			}), $("#m_login_signup_cancel").click(function (e) {
				e.preventDefault(), a()
			})
		};
	return {
		init: function () {
			l(), $("#m_login_signin_submit").click(function (e) {
				// $("#m_login_signin_submit").prop('disabled', true);
				e.preventDefault();
				var a = $(this),
					l = $(this).closest("form");
				l.validate({
					rules: {
						username: {
							required: !0,
						},
						password: {
							required: !0
						}
					}
				}), l.valid() && (a.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), l.ajaxSubmit({
                    type: 'post',
                    url: site_url + "auth/login_validation",
                    dataType: 'json',
					success: function (result) {
						$("#m_login_signin_submit").prop('disabled', true);
                        if (result.msg == 'Tidak terdaftar') {
							$("#m_login_signin_submit").prop('disabled', false);
                            setTimeout(function () {
                                a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "danger", "Akun tidak terdaftar. Silahkan coba lagi.")
                            }, 2e3);
                        } else if (result.msg == 'Gagal login') {
							$("#m_login_signin_submit").prop('disabled', false);
                            setTimeout(function () {
                                a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "danger", "Username dan password tidak benar. Harap coba logi")
                            }, 2e3);
                        } else {
                            setTimeout(function () {
                                window.location.href = result.url;
                            }, 2e3);
                        }
					}
				}))
			})
		}
	}
}();
jQuery(document).ready(function () {
	SnippetLogin.init()
});